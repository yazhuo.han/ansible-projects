## "hosts" file = Ansible Inventory File
    - "hosts" means the managed servers
    - default location for the file: /etc/ansible/hosts

## To connect to two servers on DigitalOcean using ansible:
create a file named "hosts" locally, with the content of:
    
    [droplet]
    157.245.137.168 ansible_ssh_private_key_file=~/.ssh/id_rsa ansible_user=root
    198.199.66.142 ansible_ssh_private_key_file=~/.ssh/id_rsa ansible_user=root

then use the command line:

    ansible droplet -i hosts -m ping 

## To connect to one specific server in the group "droplet"

    ansible 198.199.66.142 -i hosts -m ping

## To connect to multiple servers at one time

    [droplet]
    157.245.137.168
    198.199.66.142

    [droplet:vars]
    ansible_ssh_private_key_file=~/.ssh/id_rsa
    ansible_user=root

## Configure AWS EC2 server with Ansible
launch 2 amazon linux instances.
add to Ansible Inventory File "hosts"
    [ec2]
    ec2-34-239-144-210.compute-1.amazonaws.com
    ec2-35-175-203-210.compute-1.amazonaws.com ansible_python_interpreter=/usr/bin/python3

    [ec2:vars]
    ansible_ssh_private_key_file=~/Downloads/ansible-key-pair.pem
    ansible_user=ec2-user

ssh into the ec2 instances and execute:
    $ sudo yum install python3

connect to the server:
    $ ansible ec2 -i hosts -m ping

## Adding authorized keys or known hosts

1. When a server's authentication method is through ssh keys----adding the target server as known hosts

    cat ~/.ssh/known_hosts

    ssh-keyscan -H 198.199.66.142 >> ~/.ssh/known_hosts

To check the public key is added from local host to the server,

Locally:
    cat ~/.ssh/id_rsa.pub

On remote server 
    cat ~/.ssh/authorized_keys

2. When a server's authentication method is through password

// will take the public key from default location ~/.ssh/id_rsa.pub
// to root@157.245.137.168

    ssh-copy-id root@157.245.137.168
    provide password
    then no more password required in the further logins

Check it on the remote server:
    cat ~/.ssh/authorized_keys

## Disable host key check (less secure)--good for ephemeral infrastructure
- servers are dynamically created and destroyed

- Config File Default Locations
    - /etc/ansible/ansible.cfg
    - ~/.ansible.cfg

    $ vim ~/.ansible.cfg

    [defaults]
    host_key_checking = False

Now when we connect to a server for the first time, we shouldn't get any prompt.

Another usecase: the home directory can be a project's root directory.

