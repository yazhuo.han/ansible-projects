## Steps in playbook

## Play 1: Install node and npm
1. install node app dependencies via npm
2. start app via node command

## Play 2: Deploy nodejs app
1. copy tar file
2. unpack tar file

## Under Play 2: Run nodejs app on the server
1. install app dependencies
2. run node command

## For best practice, don't log into remote server as root
1. create a new user
2. run app using the new user

## Prameterize Playbook
1. Define variables in playbook
vars:
    - location: xxx
    - version: 1.0.0
    - destination: /home/joey

2. Passing variables in command line
    $ ansible-playbook -i hosts nodejs-deploy.yaml --extra-vars "version=1.0.0 location=xxx"

3. Using external configuration file--The Best Way!
Create a file named "vars.yaml",
Under each play, add: 
    vars_files:
      - vars.yaml
