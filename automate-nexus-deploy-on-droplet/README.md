## Steps when we did it manually:
1. create a droplet
2. ssh into server and execute:
    - download Nexus binary and unpack
    - run Nexus application using Nexus user

## The "nexus.sh" file as a reference for the command we will execute on the server, but we will fit all the commands in ansible playbook.

## Create a new file called "nexus-deploy.yaml"


## Command to run the playbook
    $ ansible-playbook -i hosts nexus-deploy.yaml

## Good Practice: name your server
It's always a good practice to name our server, even for just one server.
