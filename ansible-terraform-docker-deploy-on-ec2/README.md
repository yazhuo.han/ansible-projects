# Steps of the project
1. Create AWS EC2 instance with Terraform
2. Configure ansible inventory file to connect to AWS EC2 instance
3. On ec2 server
    - install docker and docker-compose
    - copy docker-compose file to server
    - start docker container

## 1. Create aws ec2 instance with Terraform
    $ terraform init
    $ terraform apply

## 2. Adjust Ansible Inventory File (hosts) to configure ec2 instance
added [docker_server] to inventory file

## 3 Create playbooks
1. docker-deploy.yaml
play 1: install python3 & docker & docker-compose
play 2: start docker container

## Execute the playbook

    $ ansible-playbook docker-deploy.yaml

## Run the docker-compose file

    $ docker-compose -f docker-compose.yaml up
    
## Notes on command and shell module
Only use command and shell when there is no appropriate Ansible module available.
Because both command and shell does not have state management.

## Notes on collections
ansible.builtin = default namespace and collection name
It's better to start using the fully qualified name such as "community.docker.docker_image" and not just "docker".
It's to make sure that ansible will take the right module from the right collection.

-------------

## adding a provisioner in main.tf 
After the ec2 instance created, the ansible-playbook command will be created locally.

But because the host's ip address will be newly created, we need a dynamic system to pass the new ip address of the ec2 instance.

The dynamic system is add flags --inventory, --private-key and --user in provisioner, this will overwrite the host in inventory file.

Then remember to change to hosts in playbook from docker_server to all.

Because only when ec2 instance is ready, can ansible start the configuration. We can add a play called "Wait for ssh connection" to allow ansible to wait until ec2 instance to be accessible on port 22.

Trigger: whenever there's a new instance, execute the ansible playbook
