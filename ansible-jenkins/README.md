# Steps of the Project
---set up the environment----
1. Run Jenkins on Digital Ocean server
2. Create dedicated server for Ansible
3. Install Ansible on that server

4.Execute Ansible Playbook (deploy-docker-new-user.yaml) from Jenkins pipeline to confugure two ec2 instances.

(1) create two ec2 instances
(2) configure everything from scratch with Ansible
(3) create a Jenkins pipeline
(4) connect pipeline to Java Maven App
(5) create Jenkinsfile that executes Ansible Playbook on the remote Ansible server

# Execution of the steps
Created a droplet called "ansible-server"
ssh into the server: 
    $ apt update
    # apt install ansible
    # pip3
    # apt install python3-pip
    # python3
    # pip3 install boto3
    # mkdir .aws
    # cd .aws
    # vim credentials
    Go to local computer
        $ cat .aws/credentials
        copy the credentials and paste it into the credentials file on the ansible server.

Create 2 amazon linux instances on aws. Created and downloaded new key pair "ansible-jenkins.pem"

# optimization
write a shell script for ansible server preparation