## About my-playbook.yaml
1. both tasks are executed on the hosts that blong to webserver group in the hosts file
2. task 1: install latest nginx server using apt module
3. task 2: start nginx server using service module

## Execute the playbook
    ansible-playbook -i hosts nginx-playbook.yaml

## Problem: unable to install nginx
    Solution: run apt-get update command on the servers
    $ sudo apt-get update

## Check nginx is running on the server

    $ ps aux | grep nginx
    $ nginx -v