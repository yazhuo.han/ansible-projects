## Project Steps:
1. Create K8s cluster on AWS using TF
2. Configure Ansible to connect to EKS cluster
3. Deploy Deployment and Service component inside the cluster

1. Creat eks cluster
Reuse the code from terraform projects on branch feature/provisioning-eks-cluster.

    $ terraform init
    $ terraform apply

2. Configure ansible to connect to EKS cluster
    2.1 Create a Namespace in eks cluster
    2.2 Deploy nginx in new namespace

3. Execute playbook

    $ ansible-playbook ansible-deploy-to-k8s.yaml

4. Check the app is deployed in the namespace:

    $ kubectl get pod -n my-app

5. Set environment variable for kubeconfig 
in terminal: 
    $ export K8S_AUTH_KUBECONFIG=~/DevOps/TWN_Bootcamp/Projects/ansible/ansible-terraform-deploy-on-eks/kubeconfig_myapp-eks-cluster