# branch: feature/deploy-to-ec2
vpc_cidr_block = "10.0.0.0/16"
subnet_1_cidr_block = "10.0.10.0/24"
avail_zone = "us-east-1a"
env_prefix = "prod"
my_ip = "75.8.230.66/32"
instance_type = "t2.micro"
ssh_key = "/Users/joeyhan/.ssh/id_rsa.pub"


