provider "aws" {
    region = "us-east-1"
}

variable vpc_cidr_block {}
variable private_subnet_cidr_blocks {}
variable public_subnet_cidr_blocks {}

data "aws_availability_zones" "azs" {}


module "myapp-vpc" { # we name the module "myapp-vpc"
    # source of the module, this module code will get downloaded on "terraform init"
    source = "terraform-aws-modules/vpc/aws" 
    version = "2.64.0"
    # there are 14 required attributes here
    name = "myapp-vpc"
    cidr = var.vpc_cidr_block

    # best practice: 1 private and 1 public subnet in each AZ
    private_subnets = var.private_subnet_cidr_blocks
    public_subnets = var.public_subnet_cidr_blocks

    # set AZs dynamically depending on the region
    # we use data to query aws to find the az
    azs = data.aws_availability_zones.azs.names 
    
    enable_nat_gateway = true  # default: one NAT gateway per subnet
    single_nat_gateway = true # all private subnets will route their internet traffic through this single NAT gateway
    enable_dns_hostnames = true 

    # myapp-eks-cluster is our cluster name
    # we use tags to label our resources, easy for us to know more info
    # pragramatically, label is for referencing components from other components

    tags = {
        "kubernetes.io/cluster/myapp-eks-cluster" = "shared"
    }
    
    # for the cloud native loadbalancer
    # open for external request
    public_subnet_tags = {
        "kubernetes.io/cluster/myapp-eks-cluster" = "shared"
        "kubernetes.io/role/elb" = 1 
    }
    # for the loadbalancer service
    # not open for internet
    private_subnet_tags = {
        "kubernetes.io/cluster/myapp-eks-cluster" = "shared"
        "kubernetes.io/role/internal-elb" = 1 
    }

}
